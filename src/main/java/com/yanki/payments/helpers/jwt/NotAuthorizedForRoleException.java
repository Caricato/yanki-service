package com.yanki.payments.helpers.jwt;

import com.yanki.payments.hexagonal.errors.AuthenticationException;
import com.yanki.payments.modules.user.domain.UserType;
import lombok.Getter;

@Getter
public class NotAuthorizedForRoleException extends RuntimeException implements AuthenticationException {
    private final String code = "SEC_TKN_003";
    private final String message;
    private final transient Object data;

    public NotAuthorizedForRoleException(UserType role) {
        super();
        this.message = String.format("You're not authorized to execute this service with role %d.", role.getValue());
        this.data = null;
    }
}
