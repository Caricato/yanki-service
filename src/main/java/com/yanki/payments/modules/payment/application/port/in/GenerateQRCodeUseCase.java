package com.yanki.payments.modules.payment.application.port.in;

import com.yanki.payments.modules.user.domain.UserType;
import org.springframework.web.multipart.MultipartFile;

public interface GenerateQRCodeUseCase {
    String generateQRCode(Double amount, String concept, String token, UserType userType) throws Exception;
}
