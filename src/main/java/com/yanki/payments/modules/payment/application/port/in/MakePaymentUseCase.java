package com.yanki.payments.modules.payment.application.port.in;

import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.user.domain.UserType;

public interface MakePaymentUseCase {
    Payment makePayment(String code, String token, UserType userType);
}
