package com.yanki.payments.modules.payment.adapter.port.out.persistence.repositories;

import com.yanki.payments.modules.payment.adapter.port.out.persistence.entities.QrCodeModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QrCodeRepository extends JpaRepository<QrCodeModel, Long> {
}
