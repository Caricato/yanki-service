package com.yanki.payments.modules.payment.adapter.port.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "payment")
public class PaymentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "payment_id")
    private Long idPayment;

    @Column(name = "seller_uuid")
    private UUID uuidSeller;

    @Column(name = "buyer_uuid")
    private UUID uuidBuyer;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "concept", length = 50)
    private String concept;

    @Column(name = "payment_date")
    private LocalDateTime datePayment;

    @Column(name = "payment_state")
    private Integer statePayment;

}
