package com.yanki.payments.modules.payment.adapter.port.out.persistence.mappers;

import com.yanki.payments.modules.payment.adapter.port.out.persistence.entities.QrCodeModel;
import com.yanki.payments.modules.payment.domain.QrCode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface QrCodeMapper {

    @Mapping(target = "idQr", ignore = true)
    @Mapping(source = "idPayment", target = "idPayment")
    @Mapping(source = "url", target = "qrSource")
    QrCodeModel toQrModel(QrCode qrCode);
}
