package com.yanki.payments.modules.payment.application.port.in;

import lombok.Data;

@Data
public class GenerateQRCode {
    private Double amount;
    private String concept;
}
