package com.yanki.payments.modules.payment.adapter.port.out.persistence.impl;

import com.yanki.payments.hexagonal.PersistenceAdapter;
import com.yanki.payments.hexagonal.queries.Filters;
import com.yanki.payments.hexagonal.queries.Paginator;
import com.yanki.payments.modules.payment.adapter.port.out.persistence.entities.PaymentModel;
import com.yanki.payments.modules.payment.adapter.port.out.persistence.mappers.PaymentMapper;
import com.yanki.payments.modules.payment.adapter.port.out.persistence.repositories.PaymentRepository;
import com.yanki.payments.modules.payment.application.port.out.GetPaymentByIdPort;
import com.yanki.payments.modules.payment.application.port.out.RegisterChargePort;
import com.yanki.payments.modules.payment.application.port.out.UpdatePaymentPort;
import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.user.application.port.out.GetUserPaymentHistoryPort;
import com.yanki.payments.modules.user.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class PaymentPersistenceAdapter implements RegisterChargePort, GetPaymentByIdPort, UpdatePaymentPort, GetUserPaymentHistoryPort {
    private final PaymentMapper paymentMapper;
    private final PaymentRepository paymentRepository;

    @Override
    public Payment registerCharge(Payment payment) {
        var row = paymentMapper.toPaymentModel(payment);
        var saved = paymentRepository.save(row);
        return paymentMapper.toPayment(saved);
    }

    @Override
    public Payment getPaymentById(Long idPayment) {
        var row = paymentRepository.findByIdPayment(idPayment);
        return paymentMapper.toPayment(row);
    }

    @Override
    public Payment updatePayment(Payment payment) {
        var row = paymentMapper.toPaymentModel(payment);
        var update = paymentRepository.save(row);
        return paymentMapper.toPayment(update);
    }

    @Override
    public Paginator<Payment> getUserPaymentHistory(User user, Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        Page<PaymentModel> page;
        if (user.getUserType().getValue() == 1){
            page = paymentRepository.findByUuidSellerAndStatePaymentNot(user.getUserUUID(), 0, pageable);
        }
        else{
            page = paymentRepository.findByUuidBuyer(user.getUserUUID(), pageable);
        }

        var data = page.getContent()
                .stream().map(paymentMapper::toPayment)
                .collect(Collectors.toList());
        return Paginator.<Payment>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(data)
                .build();
    }
}
