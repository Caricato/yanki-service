package com.yanki.payments.modules.payment.adapter.port.in.web;

import com.yanki.payments.helpers.jwt.NotAuthorizedForRoleException;
import com.yanki.payments.hexagonal.WebAdapter;
import com.yanki.payments.hexagonal.helper.JwtHelper;
import com.yanki.payments.modules.payment.application.port.in.GenerateQRCode;
import com.yanki.payments.modules.payment.application.port.in.MakePayment;
import com.yanki.payments.modules.payment.application.port.in.MakePaymentUseCase;
import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.user.domain.UserType;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/payment")
public class MakePaymentController {

    private final JwtHelper jwtHelper;
    private final MakePaymentUseCase paymentUseCase;

    @PutMapping(value = "/generate-qr")
    @ApiOperation("Actualizando el cobro de un vendedor con el pago del cliente")
    public Payment makePayment(@RequestBody MakePayment makePayment,
                                  @RequestHeader("Authorization") String token,
                                  @RequestHeader("role") UserType userType) throws Exception {

        var role = jwtHelper.decode(token);

        if (!role.equals(userType.getValue()))
            throw new NotAuthorizedForRoleException(UserType.VENDEDOR);

        return paymentUseCase.makePayment(makePayment.getCode(), token, userType);
    }


}
