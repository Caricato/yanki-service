package com.yanki.payments.modules.payment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class Payment {
    private Long id;
    private UUID uuidSeller;
    private UUID uuidBuyer;
    private Double amount;
    private String concept;
    private LocalDateTime datePayment;
    private PaymentState paymentState;
}
