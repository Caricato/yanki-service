package com.yanki.payments.modules.payment.application.port.in;

import lombok.Data;

@Data
public class MakePayment {
    private String code;
}
