package com.yanki.payments.modules.payment.application.port;

import com.yanki.payments.config.LocalDateTimePeruZone;
import com.yanki.payments.hexagonal.UseCase;
import com.yanki.payments.modules.payment.application.port.in.MakePaymentUseCase;
import com.yanki.payments.modules.payment.application.port.out.GetPaymentByIdPort;
import com.yanki.payments.modules.payment.application.port.out.UpdatePaymentPort;
import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.payment.domain.PaymentState;
import com.yanki.payments.modules.token.application.port.in.DecodeCheckTokenUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserDetailUseCase;
import com.yanki.payments.modules.user.domain.UserType;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class MakePaymentService implements MakePaymentUseCase {

    private final DecodeCheckTokenUseCase decodeCheckTokenUseCase;
    private final GetUserDetailUseCase getUserDetailUseCase;
    private final GetPaymentByIdPort getPaymentByIdPort;
    private final UpdatePaymentPort updatePaymentPort;

    @Override
    public Payment makePayment(String code, String token, UserType userType) {
        var tokenBody = decodeCheckTokenUseCase.decodeChangePasswordToken(token, userType);
        var email = tokenBody.getEmail();
        var phone = tokenBody.getPhone();
        var user = getUserDetailUseCase.getUserDetail(phone, email);

        var split = code.split("-");
        var idField = split[2];
        var splitId = idField.split(":");
        var id = Long.parseLong(splitId[1]);

        var payment = getPaymentByIdPort.getPaymentById(id);

        payment.setUuidBuyer(user.getUserUUID());
        payment.setDatePayment(LocalDateTimePeruZone.now());
        payment.setPaymentState(PaymentState.PAGADO);

        return updatePaymentPort.updatePayment(payment);

    }
}
