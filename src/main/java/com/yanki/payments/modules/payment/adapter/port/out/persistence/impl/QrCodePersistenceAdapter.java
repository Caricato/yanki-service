package com.yanki.payments.modules.payment.adapter.port.out.persistence.impl;

import com.yanki.payments.hexagonal.PersistenceAdapter;
import com.yanki.payments.modules.payment.adapter.port.out.persistence.mappers.QrCodeMapper;
import com.yanki.payments.modules.payment.adapter.port.out.persistence.repositories.QrCodeRepository;
import com.yanki.payments.modules.payment.application.port.out.RegisterQrCodePort;
import com.yanki.payments.modules.payment.domain.QrCode;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class QrCodePersistenceAdapter implements RegisterQrCodePort {

    private final QrCodeMapper qrCodeMapper;
    private final QrCodeRepository qrCodeRepository;

    @Override
    public void registerQrCode(QrCode qrCode) {
        var row = qrCodeMapper.toQrModel(qrCode);
        qrCodeRepository.save(row);
    }
}
