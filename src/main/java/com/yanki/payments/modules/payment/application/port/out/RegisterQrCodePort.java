package com.yanki.payments.modules.payment.application.port.out;

import com.yanki.payments.modules.payment.domain.QrCode;

public interface RegisterQrCodePort {
    void registerQrCode(QrCode qrCode);
}
