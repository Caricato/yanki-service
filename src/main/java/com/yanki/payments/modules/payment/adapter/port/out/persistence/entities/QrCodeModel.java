package com.yanki.payments.modules.payment.adapter.port.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "qr_code")
public class QrCodeModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qr_id")
    private Long idQr;

    @Column(name = "payment_id")
    private Long idPayment;

    @Column(name = "source_qr", length = 200)
    private String qrSource;

    @OneToOne
    @JoinColumn(name = "payment_id", insertable = false, updatable = false)
    private PaymentModel paymentModel;
}
