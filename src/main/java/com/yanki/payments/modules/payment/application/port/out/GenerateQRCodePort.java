package com.yanki.payments.modules.payment.application.port.out;

public interface GenerateQRCodePort {
    void generateQRCode(Double amount, String concept);
}
