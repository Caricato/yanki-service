package com.yanki.payments.modules.payment.application.port.out;

import com.yanki.payments.modules.payment.domain.Payment;

public interface RegisterChargePort {
    Payment registerCharge(Payment payment);
}
