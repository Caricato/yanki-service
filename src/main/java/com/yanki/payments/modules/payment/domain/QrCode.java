package com.yanki.payments.modules.payment.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QrCode {
    private Long idQr;
    private Long idPayment;
    private String url;
}
