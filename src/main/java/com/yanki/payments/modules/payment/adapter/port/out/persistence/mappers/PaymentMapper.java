package com.yanki.payments.modules.payment.adapter.port.out.persistence.mappers;

import com.yanki.payments.modules.payment.adapter.port.out.persistence.entities.PaymentModel;
import com.yanki.payments.modules.payment.domain.Payment;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {PaymentStateMapper.class})
public interface PaymentMapper {
    @Mapping(source = "idPayment", target = "id")
    @Mapping(source = "uuidSeller", target = "uuidSeller")
    @Mapping(source = "uuidBuyer", target = "uuidBuyer")
    @Mapping(source = "amount", target = "amount")
    @Mapping(source = "concept", target = "concept")
    @Mapping(source = "datePayment", target = "datePayment")
    @Mapping(source = "statePayment", target = "paymentState")
    Payment toPayment(PaymentModel paymentModel);

    @InheritInverseConfiguration
    @Mapping(source = "paymentState.value", target = "statePayment")
    PaymentModel toPaymentModel(Payment payment);


}
