package com.yanki.payments.modules.payment.adapter.port.out.persistence.repositories;

import com.yanki.payments.modules.payment.adapter.port.out.persistence.entities.PaymentModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<PaymentModel, Long> {
    Page<PaymentModel> findByUuidBuyer(UUID uuid, Pageable pageable);
    Page<PaymentModel> findByUuidSellerAndStatePaymentNot(UUID uuid, Integer state, Pageable pageable);
    PaymentModel findByIdPayment(Long idPayment);
}
