package com.yanki.payments.modules.payment.application.port.out;

import com.yanki.payments.modules.payment.domain.Payment;

public interface GetPaymentByIdPort {
    Payment getPaymentById(Long idPayment);
}
