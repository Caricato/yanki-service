package com.yanki.payments.modules.payment.application.port;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.yanki.payments.hexagonal.UseCase;
import com.yanki.payments.hexagonal.thirdparty.S3Adapter;
import com.yanki.payments.modules.payment.application.port.in.GenerateQRCodeUseCase;
import com.yanki.payments.modules.payment.application.port.out.RegisterChargePort;
import com.yanki.payments.modules.payment.application.port.out.RegisterQrCodePort;
import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.payment.domain.PaymentState;
import com.yanki.payments.modules.payment.domain.QrCode;
import com.yanki.payments.modules.token.application.port.in.DecodeCheckTokenUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserDetailUseCase;
import com.yanki.payments.modules.user.domain.UserType;
import lombok.RequiredArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;

@UseCase
@RequiredArgsConstructor
public class GenerateQRCodeService implements GenerateQRCodeUseCase {

    private final S3Adapter s3Adapter;
    private final DecodeCheckTokenUseCase decodeCheckTokenUseCase;
    private final GetUserDetailUseCase getUserDetailUseCase;
    private final RegisterChargePort registerChargePort;
    private final RegisterQrCodePort registerQrCodePort;

    @Override
    public String generateQRCode(Double amount, String concept, String token, UserType userType) throws Exception {


        var amountString = amount.toString();
        var tokenBody = decodeCheckTokenUseCase.decodeChangePasswordToken(token, userType);
        var email = tokenBody.getEmail();
        var phone = tokenBody.getPhone();
        var user = getUserDetailUseCase.getUserDetail(phone, email);
        var payment = Payment.builder().uuidSeller(user.getUserUUID())
                .amount(amount).concept(concept).paymentState(PaymentState.EN_PROCESO).build();
        var paymentSaved = registerChargePort.registerCharge(payment);
        var id = paymentSaved.getId().toString();

        var stringQR = String.format("Amount:%s - Concept:%S - Id:%S", amountString, concept, id);
        var imageQR = generateQRCodeImage(stringQR);
        var url = s3Adapter.saveQR(imageQR);

        var qrCode = QrCode.builder().idPayment(paymentSaved.getId()).url(url).build();

        registerQrCodePort.registerQrCode(qrCode);

        return url;

    }

    public static BufferedImage generateQRCodeImage(String stringQR) throws Exception{
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = barcodeWriter.encode(stringQR, BarcodeFormat.QR_CODE, 200, 200);
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }
}
