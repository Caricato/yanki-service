package com.yanki.payments.modules.payment.adapter.port.out.persistence.mappers;

import com.yanki.payments.modules.payment.domain.PaymentState;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PaymentStateMapper {
    default PaymentState toPaymentState(Integer state){
        switch (state){
            case 0: return PaymentState.EN_PROCESO;
            case 1: return PaymentState.PAGADO;
            default: return null;
        }
    }

}
