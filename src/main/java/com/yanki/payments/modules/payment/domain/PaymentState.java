package com.yanki.payments.modules.payment.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum PaymentState {
    EN_PROCESO(0),
    PAGADO(1);

    @Getter
    private final Integer value;
}
