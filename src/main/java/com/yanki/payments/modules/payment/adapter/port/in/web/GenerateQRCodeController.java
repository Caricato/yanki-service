package com.yanki.payments.modules.payment.adapter.port.in.web;

import com.yanki.payments.helpers.jwt.NotAuthorizedForRoleException;
import com.yanki.payments.hexagonal.WebAdapter;
import com.yanki.payments.hexagonal.helper.JwtHelper;
import com.yanki.payments.modules.payment.application.port.in.GenerateQRCode;
import com.yanki.payments.modules.payment.application.port.in.GenerateQRCodeUseCase;
import com.yanki.payments.modules.user.domain.UserType;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/payment")
public class GenerateQRCodeController {

    private final JwtHelper jwtHelper;
    private final GenerateQRCodeUseCase generateQRCodeUseCase;

    @PostMapping(value = "/generate-qr")
    @ApiOperation("Generando código QR para realizar el cobro por un concepto")
    public String generateQRCode(@RequestBody GenerateQRCode generateQRCode,
                                 @RequestHeader("Authorization") String token,
                                 @RequestHeader("role") UserType userType) throws Exception {

        var role = jwtHelper.decode(token);

        if (!role.equals(userType.getValue()))
            throw new NotAuthorizedForRoleException(UserType.CLIENTE);

        return generateQRCodeUseCase.generateQRCode(generateQRCode.getAmount(), generateQRCode.getConcept(), token, userType);
    }

}
