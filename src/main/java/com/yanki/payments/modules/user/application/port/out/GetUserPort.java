package com.yanki.payments.modules.user.application.port.out;

import com.yanki.payments.modules.user.domain.User;

import java.util.Optional;
import java.util.UUID;

public interface GetUserPort {
    Optional<User> getUser(UUID uuid);
}
