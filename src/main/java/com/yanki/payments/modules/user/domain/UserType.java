package com.yanki.payments.modules.user.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserType {
    VENDEDOR(1),
    CLIENTE(2);

    @Getter
    private final Integer value;
}
