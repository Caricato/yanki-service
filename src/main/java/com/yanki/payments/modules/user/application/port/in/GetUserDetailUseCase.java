package com.yanki.payments.modules.user.application.port.in;

import com.yanki.payments.modules.user.domain.User;

public interface GetUserDetailUseCase {
    User getUserDetail(String phone, String email);
}
