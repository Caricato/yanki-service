package com.yanki.payments.modules.user.adapter.port.out.persistence.impl;

import com.yanki.payments.hexagonal.PersistenceAdapter;
import com.yanki.payments.modules.user.adapter.port.out.persistence.mappers.DigitalWalletEntityMapper;
import com.yanki.payments.modules.user.adapter.port.out.persistence.repositories.SpringJpaDigitalWalletRepository;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class DigitalWalletPersistenceAdapter {
    private final DigitalWalletEntityMapper digitalWalletMapper;
    private final SpringJpaDigitalWalletRepository digitalWalletRepository;
}
