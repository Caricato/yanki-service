package com.yanki.payments.modules.user.application.port;

import com.yanki.payments.hexagonal.UseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserDetailUseCase;
import com.yanki.payments.modules.user.application.port.out.GetUserDetailPort;
import com.yanki.payments.modules.user.domain.User;
import com.yanki.payments.modules.user.domain.UserNotFoundException;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class GetUserDetailService implements GetUserDetailUseCase {
    private final GetUserDetailPort getUserDetailPort;

    @Override
    public User getUserDetail(String phone, String email) {
        var userOpt = getUserDetailPort.getUserDetail(phone, email);
        if (userOpt.isEmpty()) throw new UserNotFoundException(phone, email);
        return userOpt.get();
    }
}
