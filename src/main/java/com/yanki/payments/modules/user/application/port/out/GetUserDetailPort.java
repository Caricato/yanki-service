package com.yanki.payments.modules.user.application.port.out;

import com.yanki.payments.modules.user.domain.User;

import java.util.Optional;

public interface GetUserDetailPort {
    Optional<User> getUserDetail(String phone, String email);
}
