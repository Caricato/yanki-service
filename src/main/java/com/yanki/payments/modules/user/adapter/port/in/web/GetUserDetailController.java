package com.yanki.payments.modules.user.adapter.port.in.web;

import com.yanki.payments.hexagonal.WebAdapter;
import com.yanki.payments.modules.token.application.port.in.DecodeCheckTokenUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserDetailUseCase;
import com.yanki.payments.modules.user.application.port.in.UserDTO;
import com.yanki.payments.modules.user.application.port.in.UserDTOMapper;
import com.yanki.payments.modules.user.domain.UserType;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
public class GetUserDetailController {
    private final DecodeCheckTokenUseCase decodeCheckTokenUseCase;
    private final GetUserDetailUseCase getUserDetailUseCase;
    private final UserDTOMapper userDTOMapper;

    @ApiOperation("Obtener el detalle del usuario así como su saldo actual de su billetera mediante su correo y su numero celular")
    @GetMapping("")
    public UserDTO getUserDetail(@RequestHeader("Authorization") String token,
                                 @RequestHeader("role") UserType userType){
        var tokenBody = decodeCheckTokenUseCase.decodeChangePasswordToken(token, userType);
        var user = getUserDetailUseCase.getUserDetail(tokenBody.getPhone(), tokenBody.getEmail());

        return userDTOMapper.toUserDTO(user);
    }
}
