package com.yanki.payments.modules.user.adapter.port.out.persistence.impl;

import com.yanki.payments.hexagonal.PersistenceAdapter;
import com.yanki.payments.modules.user.adapter.port.out.persistence.mappers.UserEntityMapper;
import com.yanki.payments.modules.user.adapter.port.out.persistence.repositories.SpringJpaUserRepository;
import com.yanki.payments.modules.user.application.port.out.GetUserDetailPort;
import com.yanki.payments.modules.user.application.port.out.GetUserPort;
import com.yanki.payments.modules.user.domain.User;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserPersistenceAdapter implements GetUserDetailPort, GetUserPort {
    private final UserEntityMapper userMapper;
    private final SpringJpaUserRepository userRepository;

    @Override
    public Optional<User> getUserDetail(String phone, String email) {
        var entity = userRepository.findUserJpaEntityByPhoneAndEmail(phone, email);
        return entity.map(userMapper::toUser);
    }

    @Override
    public Optional<User>  getUser(UUID uuid) {
        var entity = userRepository.findUserJpaEntityByUserUUID(uuid);
        return entity.map(userMapper::toUser);
    }
}
