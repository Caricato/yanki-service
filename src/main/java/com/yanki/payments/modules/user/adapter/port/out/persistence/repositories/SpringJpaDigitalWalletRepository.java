package com.yanki.payments.modules.user.adapter.port.out.persistence.repositories;

import com.yanki.payments.modules.user.adapter.port.out.persistence.entities.DigitalWalletJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringJpaDigitalWalletRepository extends JpaRepository<DigitalWalletJpaEntity, Long> {
}
