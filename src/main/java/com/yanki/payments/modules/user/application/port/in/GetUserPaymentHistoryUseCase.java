package com.yanki.payments.modules.user.application.port.in;

import com.yanki.payments.hexagonal.queries.Filters;
import com.yanki.payments.hexagonal.queries.Paginator;
import com.yanki.payments.modules.user.domain.User;

public interface GetUserPaymentHistoryUseCase {
    Paginator<UserPaymentDTO> getUserPaymentHistory(User user, Filters filters);
}
