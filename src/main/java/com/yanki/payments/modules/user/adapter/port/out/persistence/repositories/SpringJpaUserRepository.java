package com.yanki.payments.modules.user.adapter.port.out.persistence.repositories;

import com.yanki.payments.modules.user.adapter.port.out.persistence.entities.UserJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SpringJpaUserRepository extends JpaRepository<UserJpaEntity, Long> {
    Optional<UserJpaEntity> findUserJpaEntityByPhoneAndEmail(String phone, String email);
    Optional<UserJpaEntity> findUserJpaEntityByUserUUID(UUID uuid);
}
