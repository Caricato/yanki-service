package com.yanki.payments.modules.user.adapter.port.out.persistence.mappers;

import com.yanki.payments.modules.user.adapter.port.out.persistence.entities.UserJpaEntity;
import com.yanki.payments.modules.user.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {DigitalWalletEntityMapper.class, UserTypeEntityMapper.class})
public interface UserEntityMapper {
    @Mapping(source = "userUUID", target = "userUUID")
    @Mapping(source = "userName", target = "userName")
    @Mapping(source = "userFirstLastName", target = "userFirstLastName")
    @Mapping(source = "userSecondLastName", target = "userSecondLastName")
    @Mapping(source = "digitalWallet", target = "digitalWallet")
    @Mapping(source = "type", target = "userType")
    User toUser(UserJpaEntity userEntity);
}
