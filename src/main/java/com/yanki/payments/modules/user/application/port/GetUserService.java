package com.yanki.payments.modules.user.application.port;

import com.yanki.payments.hexagonal.UseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserUseCase;
import com.yanki.payments.modules.user.application.port.out.GetUserPort;
import com.yanki.payments.modules.user.domain.User;
import com.yanki.payments.modules.user.domain.UserUUIDNotFoundException;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetUserService implements GetUserUseCase {
    private final GetUserPort getUserPort;

    @Override
    public User getUser(UUID uuid) {
        var userOpt = getUserPort.getUser(uuid);
        if (userOpt.isEmpty()) throw new UserUUIDNotFoundException(uuid);
        return userOpt.get();
    }
}
