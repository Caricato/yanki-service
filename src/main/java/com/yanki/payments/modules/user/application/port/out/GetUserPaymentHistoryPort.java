package com.yanki.payments.modules.user.application.port.out;

import com.yanki.payments.hexagonal.queries.Filters;
import com.yanki.payments.hexagonal.queries.Paginator;
import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.user.domain.User;

public interface GetUserPaymentHistoryPort {
    Paginator<Payment> getUserPaymentHistory(User user, Filters filters);
}
