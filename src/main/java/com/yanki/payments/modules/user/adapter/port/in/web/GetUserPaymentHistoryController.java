package com.yanki.payments.modules.user.adapter.port.in.web;

import com.yanki.payments.hexagonal.WebAdapter;
import com.yanki.payments.hexagonal.queries.Filters;
import com.yanki.payments.hexagonal.queries.Paginator;
import com.yanki.payments.modules.token.application.port.in.DecodeCheckTokenUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserDetailUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserPaymentHistoryUseCase;
import com.yanki.payments.modules.user.application.port.in.UserDTO;
import com.yanki.payments.modules.user.application.port.in.UserPaymentDTO;
import com.yanki.payments.modules.user.domain.UserType;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
public class GetUserPaymentHistoryController {
    private final DecodeCheckTokenUseCase decodeCheckTokenUseCase;
    private final GetUserDetailUseCase getUserDetailUseCase;
    private final GetUserPaymentHistoryUseCase getUserPaymentHistoryUseCase;

    @ApiOperation("Obtener el historico de pagos del usuario")
    @GetMapping("/payment")
    public Paginator<UserPaymentDTO> getUserPaymentHistory(@RequestHeader("Authorization") String token,
                                                           @RequestHeader("role") UserType userType,
                                                           Pageable pageable){
        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .build();
        var tokenBody = decodeCheckTokenUseCase.decodeChangePasswordToken(token, userType);
        var user = getUserDetailUseCase.getUserDetail(tokenBody.getPhone(), tokenBody.getEmail());
        return getUserPaymentHistoryUseCase.getUserPaymentHistory(user, filters);
    }
}
