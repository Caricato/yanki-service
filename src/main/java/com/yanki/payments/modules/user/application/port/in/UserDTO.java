package com.yanki.payments.modules.user.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO {
    UUID userUUID;
    String userName;
    String userFirstLastName;
    String userSecondLastName;
    Double availableAmount;
    String updateDate;
}
