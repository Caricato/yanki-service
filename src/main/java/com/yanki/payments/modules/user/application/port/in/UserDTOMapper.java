package com.yanki.payments.modules.user.application.port.in;

import com.yanki.payments.modules.user.domain.User;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public interface UserDTOMapper {
    @Mapping(source = "digitalWallet.availableAmount", target = "availableAmount")
    @Mapping(source = "userName", target = "userName")
    @Mapping(source = "userSecondLastName", target = "userSecondLastName")
    @Mapping(source = "userUUID", target = "userUUID")
    @Mapping(source = "userFirstLastName", target = "userFirstLastName")
    UserDTO toUserDTO(User user);

    @AfterMapping
    default void setUpdateDate(User user, @MappingTarget UserDTO userDTO){
        var updateDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(user.getDigitalWallet().getUpdateDate());
        userDTO.setUpdateDate(updateDateFormat);
    }
}
