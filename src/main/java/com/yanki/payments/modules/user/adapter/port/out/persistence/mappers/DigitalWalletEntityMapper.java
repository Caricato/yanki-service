package com.yanki.payments.modules.user.adapter.port.out.persistence.mappers;

import com.yanki.payments.modules.user.adapter.port.out.persistence.entities.DigitalWalletJpaEntity;
import com.yanki.payments.modules.user.domain.DigitalWallet;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DigitalWalletEntityMapper {
    @Mapping(source = "digitalWalletId", target = "digitalWalletId")
    @Mapping(source = "updateDate", target = "updateDate")
    @Mapping(source = "availableAmount", target = "availableAmount")
    @Mapping(source = "accountableAmount", target = "accountableAmount")
    DigitalWallet toDigitalWallet(DigitalWalletJpaEntity digitalWalletEntity);
}
