package com.yanki.payments.modules.user.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    UUID userUUID;
    String userName;
    String userFirstLastName;
    String userSecondLastName;
    DigitalWallet digitalWallet;
    UserType userType;

    public String fullName(){
        return userName+" "+ userFirstLastName+ " "+ userSecondLastName;
    }
}
