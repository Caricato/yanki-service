package com.yanki.payments.modules.user.application.port;

import com.yanki.payments.hexagonal.UseCase;
import com.yanki.payments.hexagonal.queries.Filters;
import com.yanki.payments.hexagonal.queries.Paginator;
import com.yanki.payments.modules.payment.domain.Payment;
import com.yanki.payments.modules.user.application.port.in.GetUserDetailUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserPaymentHistoryUseCase;
import com.yanki.payments.modules.user.application.port.in.GetUserUseCase;
import com.yanki.payments.modules.user.application.port.in.UserPaymentDTO;
import com.yanki.payments.modules.user.application.port.out.GetUserPaymentHistoryPort;
import com.yanki.payments.modules.user.domain.User;
import com.yanki.payments.modules.user.domain.UserType;
import lombok.RequiredArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@UseCase
@RequiredArgsConstructor
public class GetUserPaymentHistoryService implements GetUserPaymentHistoryUseCase {
    private final GetUserPaymentHistoryPort getUserPaymentHistoryPort;
    private final GetUserUseCase getUserUseCase;

    @Override
    public Paginator<UserPaymentDTO> getUserPaymentHistory(User user, Filters filters) {
        var payments =  getUserPaymentHistoryPort.getUserPaymentHistory(user, filters);
        var paymentsDTO = new ArrayList<UserPaymentDTO>();

        for (var payment: payments.getData()){
            String datePaymentStr;
            datePaymentStr = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss").format(payment.getDatePayment());
            var userPaymentDTO = UserPaymentDTO.builder()
                    .datePayment(datePaymentStr)
                    .amount(payment.getAmount())
                    .sellerFullName(getUserUseCase.getUser(payment.getUuidSeller()).fullName())
                    .buyerFullName(getUserUseCase.getUser(payment.getUuidBuyer()).fullName())
                    .concept(payment.getConcept()).build();
            paymentsDTO.add(userPaymentDTO);
        }

        return Paginator.<UserPaymentDTO>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(payments.getTotal())
                .data(paymentsDTO)
                .build();
    }
}
