package com.yanki.payments.modules.user.adapter.port.out.persistence.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "digital_wallet", schema = "yanki_user")
@Entity
@Data
public class DigitalWalletJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "digital_wallet_id")
    private Long digitalWalletId;

    @Column(name = "accountable_amount")
    private Double accountableAmount;

    @Column(name = "available_amount")
    private Double availableAmount;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;
}
