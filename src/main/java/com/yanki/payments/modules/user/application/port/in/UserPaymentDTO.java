package com.yanki.payments.modules.user.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPaymentDTO {
    String sellerFullName;
    String buyerFullName;
    Double amount;
    String concept;
    String datePayment;
}
