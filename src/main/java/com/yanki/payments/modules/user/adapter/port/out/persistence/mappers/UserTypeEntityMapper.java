package com.yanki.payments.modules.user.adapter.port.out.persistence.mappers;

import com.yanki.payments.modules.user.domain.UserType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserTypeEntityMapper {
    default UserType toUserType(String type){
        switch (type){
            case "VENDEDOR": return UserType.VENDEDOR;
            case "CLIENTE": return UserType.CLIENTE;
            default: return null;
        }
    }

    default UserType toUserType(Integer value){
        switch (value){
            case 1: return UserType.VENDEDOR;
            case 2: return UserType.CLIENTE;
            default: return null;
        }
    }
}
