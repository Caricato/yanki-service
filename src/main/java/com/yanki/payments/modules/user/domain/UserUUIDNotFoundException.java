package com.yanki.payments.modules.user.domain;

import com.yanki.payments.hexagonal.errors.NotFoundException;
import lombok.Getter;
import lombok.Value;

import java.util.UUID;

@Getter
public class UserUUIDNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "USR_02";
    private final String message;
    private final transient Object data;

    @Override
    public String getCode() {
        return code;
    }

    @Value
    static class Data{
        UUID uuid;
    }

    public UserUUIDNotFoundException(UUID uuid){
        super();
        this.message = String.format("User with uuid %s", uuid);
        this.data = new Data(uuid);
    }
}
