package com.yanki.payments.modules.user.adapter.port.out.persistence.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@ToString
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "user", schema = "yanki_user")
@Entity
@Data
public class UserJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "name", length = 50)
    private String userName;

    @Column(name = "father_lastname", length = 50)
    private String userFirstLastName;

    @Column(name = "mother_lastname", length = 50)
    private String userSecondLastName;

    @Column(name = "email", length = 50)
    private String email;

    @Column(name = "phone", length = 9)
    private String phone;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "type")
    private String type;

    @OneToOne
    @JoinColumn(name = "digital_wallet_id")
    private DigitalWalletJpaEntity digitalWallet;

    @Column(name = "user_uuid")
    private UUID userUUID;
}
