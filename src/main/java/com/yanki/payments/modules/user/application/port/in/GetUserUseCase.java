package com.yanki.payments.modules.user.application.port.in;

import com.yanki.payments.modules.user.domain.User;

import java.util.UUID;

public interface GetUserUseCase {
    User getUser(UUID uuid);
}
