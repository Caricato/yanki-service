package com.yanki.payments.modules.user.domain;

import com.yanki.payments.hexagonal.errors.NotFoundException;
import lombok.Getter;
import lombok.Value;

@Getter
public class UserNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "USR_01";
    private final String message;
    private final transient Object data;

    @Override
    public String getCode() {
        return code;
    }

    @Value
    static class Data{
        String phone;
        String email;
    }

    public UserNotFoundException(String phone, String email){
        super();
        this.message = String.format("User with phone number %s and email %s not found", phone, email);
        this.data = new Data(phone, email);
    }
}
