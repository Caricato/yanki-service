package com.yanki.payments.modules.token.domain;

import lombok.Value;

import java.util.HashMap;
import java.util.Map;

@Value
public class CheckTokenBody {
    String email;
    String phone;

    public Map<String, Object> map() {
        var map = new HashMap<String, Object>();
        map.put("email", email);
        map.put("phone", phone);
        return map;
    }
}
