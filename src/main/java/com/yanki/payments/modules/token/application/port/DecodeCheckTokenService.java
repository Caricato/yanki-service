package com.yanki.payments.modules.token.application.port;

import com.yanki.payments.hexagonal.UseCase;
import com.yanki.payments.hexagonal.helper.JwtHelper;
import com.yanki.payments.modules.token.application.port.in.DecodeCheckTokenUseCase;
import com.yanki.payments.modules.token.domain.CheckTokenBody;
import com.yanki.payments.modules.token.domain.InvalidTokenException;
import com.yanki.payments.modules.user.domain.UserType;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class DecodeCheckTokenService implements DecodeCheckTokenUseCase {
    private final JwtHelper jwtHelper;
    private final static Integer IS_SELLER = 1;
    private final static Integer IS_CLIENT = 2;

    @Override
    public CheckTokenBody decodeChangePasswordToken(String token, UserType role) {
        try {
            var value = jwtHelper.decode(token);
            if (value != role.getValue()) throw new Exception();
            if (value == IS_SELLER) return new CheckTokenBody("jose.javier@gmail.com", "991820122");
            else if (value == IS_CLIENT) return new CheckTokenBody("rodrigo.dulanto@gmail.com", "999871711");
            return null;
        } catch (Exception exception) {
            throw new InvalidTokenException();
        }
    }
}
