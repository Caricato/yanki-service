package com.yanki.payments.modules.token.application.port.in;

import com.yanki.payments.modules.token.domain.CheckTokenBody;
import com.yanki.payments.modules.user.domain.UserType;

public interface DecodeCheckTokenUseCase {
    CheckTokenBody decodeChangePasswordToken(String token, UserType role);
}
