package com.yanki.payments.hexagonal.thirdparty;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.yanki.payments.config.StorageCredentials;
import com.yanki.payments.hexagonal.StorageAdapter;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

@Data
@StorageAdapter
@RequiredArgsConstructor
public class S3Adapter {

    private final StorageCredentials storageCredentials;
    private final Logger logger = LoggerFactory.getLogger(S3Adapter.class);

     public AmazonS3 getS3Client(){
            var awsCredentials = new BasicAWSCredentials(
                    storageCredentials.getCredentials().getAccessKey(),
                    storageCredentials.getCredentials().getSecretKey()
            );
            return AmazonS3ClientBuilder.standard()
                    .withRegion(storageCredentials.getRegion())
                    .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                    .build();
    }

    public String saveQR(BufferedImage bufferedImage) throws IOException, NullPointerException {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "png", os);
            byte[] buffer = os.toByteArray();
            InputStream is = new ByteArrayInputStream(buffer);
            ObjectMetadata metadata = new ObjectMetadata();
//        metadata.setContentType("image/png");
            metadata.setContentLength(buffer.length);
            var request = new PutObjectRequest(storageCredentials.getBucket().getName(),"qr.png",is,metadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead);
            this.getS3Client().putObject(request);
            return this.getS3Client().getUrl(storageCredentials.getBucket().getName(),"qr.png").toString();
        } catch (Exception e) {
            logger.error("An error occurred while saving the code QR to the AWS S3 service", e);
            return null;
        }

    }
}
