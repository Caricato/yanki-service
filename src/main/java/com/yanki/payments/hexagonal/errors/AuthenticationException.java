package com.yanki.payments.hexagonal.errors;
/**
 * Authentication exception should be used for errors with tokens and login
 **/

public interface AuthenticationException extends BusinessException {
}