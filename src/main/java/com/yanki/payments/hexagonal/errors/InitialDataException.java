package com.yanki.payments.hexagonal.errors;
/**
 * Initial data exception not loaded in the database.
 **/

public interface InitialDataException extends BusinessException{
}
