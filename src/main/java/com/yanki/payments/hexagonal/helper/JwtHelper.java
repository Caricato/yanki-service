package com.yanki.payments.hexagonal.helper;

import com.yanki.payments.config.JwtConfig;
import com.yanki.payments.config.LocalDateTimePeruZone;
import com.yanki.payments.hexagonal.HelperAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;

import java.security.Key;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Map;

@HelperAdapter
@RequiredArgsConstructor
public class JwtHelper {

    private static final String TOKEN_SELLER = "BvPHGM8C0ia4uOuxxqPD5DTbWC9F9TWvPStp3pb7ARo0oK2mJ3pd3YG4lxA9i8bj6OTbadwezxgeEByY";
    private static final String TOKEN_CLIENT = "sdFKAmC0ia4uOuxxqAx0DTbKFm1203vaCtp3pb7ARo0kv120MaskdsMa9i81Mv0bj6OTbadwezxb12XK";
    private final JwtConfig jwtConfig;

    public Integer decode(String jwt) throws Exception {
        if (jwt.contains(TOKEN_SELLER)) return 1;
        else if (jwt.contains(TOKEN_CLIENT)) return 2;
        throw new Exception();
    }

    public String generateToken(String username, Map<String, Object> claims, Long expirationTimeInMinutes) {
        final var key = getKey();
        var createdDate = LocalDateTimePeruZone.now();
        var expirationDate = createdDate.plusMinutes(expirationTimeInMinutes);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(Date.from(createdDate.toInstant(ZoneOffset.UTC)))
                .setExpiration(Date.from(expirationDate.toInstant(ZoneOffset.UTC)))
                .signWith(key)
                .compact();
    }

    private Key getKey() {
        return Keys.hmacShaKeyFor(jwtConfig.getSecret().getBytes());
    }
}
