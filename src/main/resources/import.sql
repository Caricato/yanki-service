INSERT INTO yanki_user.digital_wallet(accountable_amount, available_amount, creation_date, update_date) VALUES (10240.00, 10240.00, '2021-05-05', '2021-03-05');

COMMIT;

INSERT INTO yanki_user."user"(creation_date, email, phone, update_date, father_lastname, name, mother_lastname, user_uuid, digital_wallet_id, type) VALUES ('2021-05-05', 'jose.javier@gmail.com', '991820122', '2021-06-06', 'JAVIER', 'JOSE', 'MORALES', 'beedd237-382a-4c14-896b-56e9f5046ee3', 1, 'VENDEDOR');

COMMIT;

INSERT INTO yanki_user.digital_wallet(accountable_amount, available_amount, creation_date, update_date) VALUES (100.00, 100.00, '2021-03-03', '2021-03-04');

COMMIT;

INSERT INTO yanki_user."user"(creation_date, email, phone, update_date, father_lastname, name, mother_lastname, user_uuid, digital_wallet_id, type) VALUES ('2021-05-05', 'rodrigo.dulanto@gmail.com', '999871711', '2021-07-07', 'DULANTO', 'RODRIGO', 'CHICANA', '8e84b414-e21f-4c3a-b53c-a58114785983', 2, 'CLIENTE');

COMMIT;

INSERT INTO payment.payment(amount, concept, buyer_uuid, seller_uuid, payment_date, payment_state) VALUES (102.5, 'Tangente','8e84b414-e21f-4c3a-b53c-a58114785983', 'beedd237-382a-4c14-896b-56e9f5046ee3', '2021-12-13 05:37:51.000000', 1);

COMMIT;